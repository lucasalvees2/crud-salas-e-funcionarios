const express = require ('express');
const cors = require('cors');
const app = express();
app.use(express.json());
  

//Rotas
    app.use('/', require("./routes"));
//Outros
    app.use(cors());

    //app.use(express.static(__dirname +'/client'))

const PORT = process.env.PORT || 3001
app.listen(PORT);