const express = require('express')
const routes = express.Router();
const SalaController = require('./Controllers/SalaController')
const FuncionarioController = require('./Controllers/FuncionarioController')
routes.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

//Rotas de sala
routes.get("/sala", SalaController.index);
routes.get("/sala/:id", SalaController.busca);
routes.post("/sala", SalaController.novo);
//Rotas de funcionario
routes.get("/func", FuncionarioController.index);
routes.get("/func/:id", FuncionarioController.busca)
routes.post("/func", FuncionarioController.novo);


module.exports = routes;