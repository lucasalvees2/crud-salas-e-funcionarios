const Sequelize = require('sequelize');

const sequelize = new Sequelize('trabalho','lucas','123456',{
    host:'localhost',
    dialect: 'mysql'
});

sequelize.authenticate().then(function(){
    console.log('Conexao realizada com sucesso');
}).catch(function(err){
    console.log('Erro ao realizar a conexão com BD: '+err);
});

module.exports={
    Sequelize: Sequelize,
    sequelize: sequelize
}