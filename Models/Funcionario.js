const db = require('./db')

const Funcionario = db.sequelize.define('funcionarios',{
    nome:{
        type: db.Sequelize.STRING
    },
    matricula:{
        type: db.Sequelize.STRING
    },
    tipo:{
        type: db.Sequelize.STRING
    },
    status:{
        type: db.Sequelize.BOOLEAN
    }
})

//Funcionario.sync({force: true})

module.exports = Funcionario