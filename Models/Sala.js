const db = require('./db')

const Sala = db.sequelize.define('salas',{
    tipo:{
        type: db.Sequelize.STRING
    },
    numcad:{
        type: db.Sequelize.INTEGER
    },
    unidade:{
        type: db.Sequelize.INTEGER
    },
    status:{
        type: db.Sequelize.BOOLEAN
    }
})

//Sala.sync({force: true})

module.exports = Sala