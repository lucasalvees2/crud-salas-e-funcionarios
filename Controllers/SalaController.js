const Sala = require('../Models/Sala');

module.exports = {
    async index(req,res){
        const sala = await Sala.findAll();
        return res.json(sala);
    },

    async novo(req,res){
        const sala = await Sala.create(req.body);
        return res.json(sala);
    },

    async busca(req,res){
        const sala = await Sala.findByPk(req.params.id);
        return res.json(sala);
    }
};