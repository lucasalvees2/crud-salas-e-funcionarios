const Funcionario = require('../Models/Funcionario')

module.exports = {
    async index(req,res){
        const funcionario = await Funcionario.findAll();
        return res.json(funcionario);
    },

    async novo(req,res){
        const funcionario = await Funcionario.create(req.body);
        return res.json(funcionario);
    },

    async busca(req,res){
        const funcionario = await Funcionario.findByPk(req.params.id);
        return res.json(funcionario);
    }
};